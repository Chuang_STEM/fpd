fpd package
===========

Submodules
----------

fpd\.AlignNR\_class module
--------------------------

.. automodule:: fpd.AlignNR_class
    :members:
    :undoc-members:
    :show-inheritance:

fpd\.dpc\_explorer\_class module
--------------------------------

.. automodule:: fpd.dpc_explorer_class
    :members:
    :undoc-members:
    :show-inheritance:

fpd\.fft\_tools module
----------------------

.. automodule:: fpd.fft_tools
    :members:
    :undoc-members:
    :show-inheritance:

fpd\.fpd\_file module
---------------------

.. automodule:: fpd.fpd_file
    :members:
    :undoc-members:
    :show-inheritance:

fpd\.fpd\_io module
-------------------

.. automodule:: fpd.fpd_io
    :members:
    :undoc-members:
    :show-inheritance:

fpd\.fpd\_processing module
---------------------------

.. automodule:: fpd.fpd_processing
    :members:
    :undoc-members:
    :show-inheritance:

fpd\.gwy module
---------------

.. automodule:: fpd.gwy
    :members:
    :undoc-members:
    :show-inheritance:

fpd\.mag\_tools module
----------------------

.. automodule:: fpd.mag_tools
    :members:
    :undoc-members:
    :show-inheritance:

fpd\.ransac\_tools module
-------------------------

.. automodule:: fpd.ransac_tools
    :members:
    :undoc-members:
    :show-inheritance:

fpd\.segmented\_dpc module
--------------------------

.. automodule:: fpd.segmented_dpc
    :members:
    :undoc-members:
    :show-inheritance:

fpd\.segmented\_dpc\_class module
---------------------------------

.. automodule:: fpd.segmented_dpc_class
    :members:
    :undoc-members:
    :show-inheritance:

fpd\.synthetic\_data module
---------------------------

.. automodule:: fpd.synthetic_data
    :members:
    :undoc-members:
    :show-inheritance:

fpd\.tem\_tools module
----------------------

.. automodule:: fpd.tem_tools
    :members:
    :undoc-members:
    :show-inheritance:

fpd\.utils module
-----------------

.. automodule:: fpd.utils
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: fpd
    :members:
    :undoc-members:
    :show-inheritance:
